import axios from 'axios'

const POST_ADMIN = async (params, path, base_path = BASE_URI + VERSION) => {
  let url = base_path + path;
  let res = await axios.post(url, params, BASE_HEADERS).catch(error => {
    return {
      is_error: true,
      msg: error
    }
  });

  let not_auth_msg = 'verify cookie failed: http: named cookie not present';
  if( res.data && erros[res.data.errMsg] ){
    app.$router.push({
      'name': 'manage_admin_login'
    })
  }
  return res.data;
}

/*
  params:
    product_id string
    block_hash string
    data_hash string
    record_time string
*/
export const add_hash_to_chain = async (params) => {
  const path = '/product_hash/add_product_chain_hash.do'
  let res = await axios.post(path, params).catch(error => {
    return {
      'data': {
        is_error: true,
        msg: error
      }
    }
  });
  return res.data;
}

// test code
// add_hash_to_chain({
//  "product_id": new Date().getTime(),
//  "block_hash": new Date().getTime(),
//  "data_hash" : new Date().getTime(),
//  "data": JSON.stringify({
//    'goddsCode': new Date().getTime(),
//    'cool': 'new Date().getTime(),'
//  }),
//  "record_time": "2020-05-12 15:23:30"
// })

/*
  params:
    page int
*/
export const query_product_hash = async (params) => {
  const path = '/product_hash/query_product_hash.do'
  let res = await axios.post(path, params).catch(error => {
    return {
      'data': {
        is_error: true,
        msg: error
      }
    }
  });
  return res.data
}

// query_product_hash
