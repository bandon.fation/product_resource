import axios from 'axios'

import {
  update_token,
  get_token
} from '@/modules/stores'

import {
  app
} from '@/controller'

// const BASE_URI = 'http://192.168.1.139:22222';
// const BASE_URI = 'http://127.0.0.1:22222'
// const BASE_URI = 'http://api.shuyuchain.com:18082';
const BASE_URI = '';
const VERSION  = '/v1';
const BASE_HEADERS = {
  'Acces-Control-Allow-Origin': '*',
  'accept': 'application/json',
  'content-type': 'application/json',
}

const erros = {
  'verify cookie failed: http: named cookie not present': 'not_auth',
  'verify token failed: Expired token': 'not_auth',
  '401': 'not_auth'
}

export const action_dict = {
  30301: '一级装箱',
  30302: '二级装箱',
  40301: '一级拆箱',
  40302: '二级拆箱',
  10100: '品牌商出库',
  10200: '零售商出库',
  10300: '中转仓库出库',
  10400: '海关出库',
  20100: '品牌方入库',
  20200: '零售商入库',
  20300: '中转仓库入库',
  20400: '海关入境',
  20500: '消费者收货',
}

const POST = async (params, path, base_path = BASE_URI + VERSION) => {
  let url = base_path + path;
  let res = await axios.post(url, params, BASE_HEADERS).catch(error => {
    return {
      is_error: true,
      msg: error
    }
  });

  let not_auth_msg = 'verify cookie failed: http: named cookie not present';
  if( res.data && erros[res.data.errMsg] ){
    app.$router.push({
      'name': 'login'
    })
  }
  return res.data;
}

const POST_ADMIN = async (params, path, base_path = BASE_URI + VERSION) => {
  let url = base_path + path;
  let res = await axios.post(url, params, BASE_HEADERS).catch(error => {
    return {
      is_error: true,
      msg: error
    }
  });

  let not_auth_msg = 'verify cookie failed: http: named cookie not present';
  if( res.data && erros[res.data.errMsg] ){
    app.$router.push({
      'name': 'manage_admin_login'
    })
  }
  return res.data;
}

const get_location = async () => {
  navigator.geolocation.getCurrentPosition(function(position) {
    console.log(position);
    alert('end');
    // do_something(position.coords.latitude, position.coords.longitude);
  });
}

// get_location();
/*
params
  struct {skuID}
  type
    skuId String
return
  struct

*/
export const get_sku_info = async (params) => {
  return await POST(params, '/consumer/skuInfo');
}

export const query_goods_hash = async (params) => {
  return await POST(params, '/consumer/certificate');
}

export const query_has_by_skuid = async (params) => {
  return await POST(params, '/consumer/certificateBySku')
}

export const get_consumer_record = async (params) => {
  return await POST(params, '/consumer/records')
}

export const goods_chain_info = async (params) => {
  return await POST(params, '/consumer/chainInfo')
}

export const consumer_report = async (params) => {
  return await POST(params, '/consumer/report')
}

export const upload_pic = async (params) => {
  return await POST(params, '/business/uploadPic')
}

export const login = async (params) => {
  return await POST(params, '/business/login')
}

export const delete_goods = async (params) => {
  return await POST(params, '/business/deleteGoods')
}

export const query_goods = async (params) => {
  return await POST(params, '/business/getGoodsInfo')
}

export const query_goods_page = async (params) => {
  return await POST(params, '/business/getGoodsPageNum')
}

export const add_goods = async (params) => {
  return await POST(params, '/business/addGoods')
}

// modifyGoods
export const update_goods = async (params) => {
  return await POST(params, '/business/modifyGoods')
}

export const change_password = async (params) => {
  return await POST(params, '/business/chPswd')
}

export const get_sku_by_goodsid = async (params) => {
  return await POST(params, '/business/getRecords')
}

export const get_sku_page = async (params) => {
  return await POST(params, '/business/getRecordsPageNum')
}

export const create_sku_code = async (params) => {
  return await POST(params, '/business/genSku')
}

export const company_login = async (params) => {
  return await POST_ADMIN(params, '/company/login')
}

export const company_change_password = async (params) => {
  return await POST_ADMIN(params, '/company/chPswd')
}

export const company_info = async (params) => {
  return await POST_ADMIN(params, '/company/getBusinessInfo')
}

export const add_company = async (params) => {
  return await POST_ADMIN(params, '/company/addBusiness')
}

export const query_company_page = async (params) => {
  return await POST_ADMIN(params, '/company/getBusinessPageNum')
}

export const modify_username = async (params) => {
  return await POST_ADMIN(params, '/company/modifyUserName')
}

export const forbidden_account = async (params) => {
  return await POST_ADMIN(params, '/company/operateAccount')
}

export const reset_password = async (params) => {
  return await POST_ADMIN(params, '/company/resetPswd'); 
}

// operateAccount

export const get_info = async (params = {}) => {
  let url = BASE_URI + VERSION + '';
  // axios.defaults.headers.common['Authorization'] = '';
  let res = await axios.get(url, params).catch(error => {
    throw error;
  });
  return res;
}

export * from './product_hash'

